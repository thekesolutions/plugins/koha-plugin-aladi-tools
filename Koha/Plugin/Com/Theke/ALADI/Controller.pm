package Koha::Plugin::Com::Theke::ALADI::Controller;

# Copyright 2022 Theke Solutions
#
# This file is part of koha-plugin-aladi-tools.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This program comes with ABSOLUTELY NO WARRANTY;

use Modern::Perl;

use Mojo::Base 'Mojolicious::Controller';

use Encode;
use File::Basename;
use File::Temp;
use List::MoreUtils qw(any);

use ALADI::DublinCore;
use ALADI::Local;

use Koha::Biblios;

=head1 Koha::Plugin::Com::Theke::ALADI::Controller

A class implementing the controller code for ALADI requests

=head2 Class methods

=head3 get_dublincore

Method that returns the DublinCore representations of a record for using on the
institutional repository.

=cut

sub get_dublincore {
    my $c = shift->openapi->valid_input or return;

    my $biblio = Koha::Biblios->find($c->validation->param('biblio_id'), { prefetch => 'metadata' });

    unless ($biblio) {
        return $c->render(
            status  => 404,
            openapi => { error => "Object not found" }
        );
    }

    return try {

        my $record = $biblio->metadata->record;

        my $dc = ALADI::DublinCore->new;

        return $c->render(
            status  => 200,
            data    => Encode::encode_utf8($dc->process({ record => $record })),
            format  => 'xml',
        );
    }
    catch {

        return $c->render(
            status  => 500,
            openapi => { error => "Unhandled exception: $_" }
        );
    };
}

=head3 get_local

Method that returns the DublinCore representations of a record for using on the
institutional repository.

=cut

sub get_local {
    my $c = shift->openapi->valid_input or return;

    my $biblio = Koha::Biblios->find($c->validation->param('biblio_id'), { prefetch => 'metadata' });

    unless ($biblio) {
        return $c->render(
            status  => 404,
            openapi => { error => "Object not found" }
        );
    }

    return try {

        my $record = $biblio->metadata->record;

        my $dc = ALADI::Local->new;

        return $c->render(
            status  => 200,
            data    => Encode::encode_utf8($dc->process({ record => $record })),
            format  => 'xml',
        );
    }
    catch {

        return $c->render(
            status  => 500,
            openapi => { error => "Unhandled exception: $_" }
        );
    };
}

=head3 get_bundle

Method that returns a zip file, suitable for importing in DSpace.

=cut

sub get_bundle {
    my $c = shift->openapi->valid_input or return;

    my $biblio = Koha::Biblios->find($c->validation->param('biblio_id'), { prefetch => 'metadata' });

    unless ($biblio) {
        return $c->render(
            status  => 404,
            openapi => { error => "Object not found" }
        );
    }

    return try {

        my $biblio_id = $biblio->id;
        my $record = $biblio->metadata->record;

        my $fh = File::Temp->new;

        my $tmp_dir  = $fh->newdir( CLEANUP => 0 );
        my $dest_dir = "$tmp_dir/$biblio_id";

        unless ( mkdir( $dest_dir ) ) {
            return $c->render(
                status  => 500,
                openapi => { error => "Error creating directory ($dest_dir)" }
            );
        }

        # DublinCore
        my $dc = ALADI::DublinCore->new;

        open( my $dc_handle, ">:encoding(UTF-8)", "$dest_dir/dublin_core.xml")
            or die "Can't save > $dest_dir/dublin_core.xml: $!";
        print $dc_handle $dc->process({ record => $record });
        close($dc_handle);

        # Local metadata
        my $local = ALADI::Local->new;

        open( my $local_handle, ">:encoding(UTF-8)", "$dest_dir/metadata_local.xml")
            or die "Can't save > $dest_dir/metadata_local.xml: $!";
        print $local_handle $local->process({ record => $record });
        close( $local_handle );

        open( my $license_handle, ">:encoding(UTF-8)", "$dest_dir/license.txt")
            or die "Can't save > $dest_dir/license.txt: $!";
        print $license_handle license();
        close($license_handle);

        open( my $contents_handle, ">>:encoding(UTF-8)", "$dest_dir/contents")
            or die "Can't save > $dest_dir/contents: $!";
        print $contents_handle "license.txt\tbundle:LICENSE\n";
        close($contents_handle);

        system( "cd $tmp_dir; zip $biblio_id\.zip -r $biblio_id" );

        $c->res->headers->content_disposition("attachment; filename=$biblio_id\.zip;");
        return $c->reply->file( "$tmp_dir/$biblio_id\.zip" );
    }
    catch {

        $c->unhandled_exception($_);
    };
}

=head3 get_urls

Method that returns attached URLs.

=cut

sub get_urls {
    my $c = shift->openapi->valid_input or return;

    my $biblio = Koha::Biblios->find( $c->validation->param('biblio_id'),
        { prefetch => 'metadata' } );

    unless ($biblio) {
        return $c->render(
            status  => 404,
            openapi => { error => "Object not found" }
        );
    }

    return try {

        my $record = $biblio->metadata->record;

        my @response;

        my @fields_856 = $record->field('856');
        foreach my $field_856 (@fields_856) {
            my $u = $field_856->subfield('u');
            my $x = $field_856->subfield('x');

            next unless $u;
            next unless $u =~ m{http://www2.aladi.org/biblioteca};

            push @response,
              {
                url        => $u,
                restricted => ( $x and $x eq 'G' )
                ? Mojo::JSON->false
                : Mojo::JSON->true
              };
        }

        return $c->render(
            status  => 200,
            openapi => \@response,
        );
    }
    catch {

        return $c->render(
            status  => 500,
            openapi => { error => "Unhandled exception: $_" }
        );
    };
}

sub get_urls_new {
    my $c = shift->openapi->valid_input or return;

    my $biblio = Koha::Biblios->find( $c->validation->param('biblio_id'),
        { prefetch => 'metadata' } );

    unless ($biblio) {
        return $c->render(
            status  => 404,
            openapi => { error => "Object not found" }
        );
    }

    return try {

        my $record = $biblio->metadata->record;

        my @response;

        my @fields_856 = $record->field('856');
        my @urls;
        my @files;

        foreach my $field ( @fields_856 ) {

            my $u = $field->subfield('u');
            my $x = $field->subfield('x');

            if ( $u and $u =~ m/^http:\/\/www2.aladi.org/ ) {

                my $file_name;

                if ( any { $u eq $_ } @urls) {
                    warn $biblio->id . "\tSKIPPED duplicate URL\t$u";
                    next;
                }

                push @urls, $u;

                my $file = basename($u);
                if ( any { $_ =~ /$file/i } @files ) {
                    my $lang = 'Undefined';
                    if ( $u =~ /\/PT\// ) {
                        $lang = 'pt';
                    }
                    elsif ( $u =~ /\/ES\// ) {
                        $lang = 'es';
                    }

                    push @files, $file;
                    my ($new_file_base, $path, $extension) = fileparse("$file", qr/\.[^.]*/);
                    $file_name = $new_file_base . '_' . $lang . $extension;
                }
                else {
                    push @files, $file;
                    $file_name = $file;
                }

                push @response, {
                    file_name  => $file_name,
                    url        => $u,
                    restricted => ( $x and $x eq 'G' )
                    ? Mojo::JSON->false
                    : Mojo::JSON->true
                }
            }
            else {
                warn $biblio->id . "\tSKIPPED\t$u";
            }
        }

        return $c->render(
            status  => 200,
            openapi => \@response,
        );
    }
    catch {

        return $c->render(
            status  => 500,
            openapi => { error => "Unhandled exception: $_" }
        );
    };
}

sub license {
    my $license = <<LICENSE;
Licencia de Distribución no exclusiva de los documentos en el Repositorio Institucional de la ALADI.


1. Declaración de titularidad

La ALADI, declara que los Documentos Oficiales de su Repositorio, constituyen trabajos originales, que no infringen los derechos de autor de ninguna otra persona o entidad.

En caso de ser co-titular, declara que cuenta con el consentimiento de los restantes titulares para hacer la presente cesión.

En caso de previa cesión de derechos de explotación sobre la obra a terceros, declara que tien la autorización expresa de aquellos a los fines de esta cesión, conservando la facultad de ceder estos derechos. Además, deja constancia, que se han adquirido todos los derechos o autorizaciones necesarias por las contribuciones de terceros al contenido del trabajo y que éstos están claramente identificados y reconocidos en el mismo.

En el caso de que el documento haya sido subsidiado por acuerdos con otros Organismos distintos a la ALALDI, declara que cumple con las obligaciones exigidas por tales acuerdos.

2. Derechos patrimoniales de los Documentos Oficiales cedidos al Repositorio Institucional de la ALADI

Para que el Repositorio Institucional de la ALADI pueda reproducir y comunicar públicamente su acervo documental, la ALADI cumple con las siguientes condiciones:

-> Como autor, la ALADI tiene el derecho gratuito y no exclusivo de archivar, reproducir, convertir, comunicar y/o distribuir los Documentos Oficiales a nivel internacional, en formato electrónico.

-> Se acuerda que la ALADI pueda conservar más de una copia de los documentos y, sin alterar su contenido, convertirlo a cualquier formato de fichero, medio o soporte, para propósitos de seguridad, preservación y acceso.

3. Condiciones de uso

Los documentos que integran el Repositorio Institucional de la ALADI pueden ser de dos categorías:

a) Aquellos de uso interno o "Restringidos para uso exclusivo de las Representaciones"; y
b) Aquellos que poseen licencias para abrir los archivos al uso público.

Estos últimos estarán disponibles al público para que se haga un uso justo y respetuoso de los derechos de autor, siendo requisito indispensable, citar la fuente, reconocer la autoría de quien firma los documentos y cumplir con las condiciones de la Licencia de Distribución No Exclusiva (Licencia Creative Commons).

Abril de 2022.

LICENSE

    return $license;
}

1;
