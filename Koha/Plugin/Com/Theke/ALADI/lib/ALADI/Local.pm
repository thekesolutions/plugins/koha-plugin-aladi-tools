package ALADI::Local;

use Modern::Perl;

use utf8;

use DateTime;
use Encode;
use List::MoreUtils qw(any none);
use XML::Writer;

use base qw(Class::Accessor);

__PACKAGE__->mk_accessors(qw(language record xml open));

=head1 NAME

ALADI::DublinCore

=head1 API

=head2 Class methods

=head3 process

    my $xml = $dc->process({ record => $record });

Given a I<MARC::Record> object returns the translated XML.

=cut

sub process {
    my ( $self, $params ) = @_;

    die "'record' is mandatory"
      unless $params->{record} and ref( $params->{record} ) eq 'MARC::Record';

    $self->record( $params->{record} );

    # FIXME: Clearly
    $self->language('WARNING');

    # initialize a fresh XML object
    $self->xml(
        XML::Writer->new(
            OUTPUT      => 'self',
            DATA_MODE   => 1,
            DATA_INDENT => 2,
        )
    );

    $self->xml->xmlDecl( 'UTF-8', 'no' );
    $self->xml->startTag( 'dublin_core', schema => 'local' );

    $self->add_single_subfield(
        {
            element   => 'koha',
            qualifier => '856u',
            field     => '856',
            subfield  => 'u',
        }
    );

    $self->add_single_subfield(
        {
            element   => 'koha',
            qualifier => 'restriccion856x',
            field     => '856',
            subfield  => 'x',
        }
    );

    $self->xml->endTag('dublin_core');

    return $self->xml->end();
}

=head2 Internal methods

=head3 add_element

    $self->add_element(
        {
            element   => $element,
            qualifier => $qualifier,
            value     => $value,
          [ language  => $language,
            default_language => 0/1 ]
        }
    );

=cut

sub add_element {
    my ( $self, $params ) = @_;

    foreach my $param (qw(element qualifier value)) {
        die "$param is mandatory"
          unless exists $params->{$param};
    }

    my $language;

    if ( $params->{language} ) {
        $language = $params->{language};
    }
    elsif ( $params->{default_language} ) {
        $language = $self->language;
    }

    if ( $language ) {
        $self->xml->startTag(
            'dcvalue',
            element   => $params->{element},
            qualifier => $params->{qualifier},
            language  => $language
        );
    }
    else {
        $self->xml->startTag(
            'dcvalue',
            element   => $params->{element},
            qualifier => $params->{qualifier},
        );
    }
    $self->xml->characters( $params->{value} );
    $self->xml->endTag('dcvalue');

    return $self;
}

=head3 add_single_subfield

    $self->add_single_subfield(
        {
            element   => $element,
            qualifier => $qualifier,
            field     => $field,
            subfield  => $subfield,
          [ language  => $language,
            default_language => 1/0 ]
        }
    );

=cut

sub add_single_subfield {
    my ( $self, $params ) = @_;

    foreach my $param (qw(element qualifier field subfield)) {
        die "$param is mandatory"
          unless exists $params->{$param};
    }

    my @fields = $self->record->field( $params->{field} );

    foreach my $field (@fields) {

        my $value = $field->subfield( $params->{subfield} );

        next unless $value;

        $self->add_element(
            {   element   => $params->{element},
                qualifier => $params->{qualifier},
                value     => $value,
                language  => $params->{language},
                default_language => $params->{default_language}
            }
        );
    }

    return $self;
}

1;
