package ALADI::DublinCore;

use Modern::Perl;

use utf8;

use DateTime;
use Encode;
use List::MoreUtils qw(any none);
use XML::Writer;

use Koha::MetadataRecord::Authority;

use base qw(Class::Accessor);

__PACKAGE__->mk_accessors(qw(language record xml open));

=head1 NAME

ALADI::DublinCore

=head1 API

=head2 Class methods

=head3 process

    my $xml = $dc->process({ record => $record });

Given a I<MARC::Record> object returns the translated XML.

=cut

sub process {
    my ( $self, $params ) = @_;

    die "'record' is mandatory"
      unless $params->{record} and ref( $params->{record} ) eq 'MARC::Record';

    $self->record( $params->{record} );

    # FIXME: Clearly
    $self->language('WARNING');
    $self->set_open;

    # initialize a fresh XML object
    $self->xml(
        XML::Writer->new(
            OUTPUT      => 'self',
            DATA_MODE   => 1,
            DATA_INDENT => 2,
        )
    );

    $self->xml->xmlDecl( 'UTF-8', 'no' );
    $self->xml->startTag( 'dublin_core', schema => 'dc' );

    $self->add_contributor_author;

    foreach my $e ( sort keys %{ $self->contributor_mapping } ) {
        $self->add_contributor($e);
    }

    $self
      ->add_contributor_other
      ->add_contributor_conference
      ->add_coverage_spatial
      ->add_coverage_temporal
      ->add_date_norspa
      ->add_description
      ->add_description_abstract
      ->add_description_tableofcontents
      ->add_33x('336')
      ->add_33x('337')
      ->add_33x('338')
      ->add_single_subfield( { element => 'identifier', qualifier => 'codaladi',  field => '082', subfield => 'a' } )
      ->add_single_subfield( { element => 'identifier', qualifier => 'codaladi2', field => '090', subfield => 'a' } )
      ->add_single_subfield( { element => 'identifier', qualifier => 'codaladi3', field => '690', subfield => 'a' } )
      ->add_single_subfield( { element => 'identifier', qualifier => 'issn',     field => '022', subfield => 'a' } )
      ->add_identifier_isbn
      ->add_identifier_other
      ->add_single_subfield( { element => 'publisher', qualifier => undef,     field => '260', subfield => 'b' } )
      ->add_single_subfield( { element => 'relation',  qualifier => undef,     field => '699', subfield => 'a' } )
      ->add_type
      ->add_type_local
      ->add_type_oaireResourceType
      ->add_element({ element => 'type', qualifier => 'version', value => 'publishedVersion', language => 'en' })
      ->add_language_iso
      ->add_rights
      ->add_single_subfield({ element => 'subject', qualifier => 'codaladi',  field => '082', subfield => 'a' })
      ->add_single_subfield({ element => 'subject', qualifier => 'codaladi2', field => '697', subfield => 'b' })
      ->add_title
      ->add_title_alternative
      ->add_single_subfield({ element => 'description', qualifier => 'statementofresponsibility', field => '245', subfield => 'c', language => 'es' })
      ->add_format_extent
      ->add_current_date
      ->add_date_issued
      ->add_subject
      ->add_subject_genreform
      ->add_relation_ispartofseries
      ->add_single_subfield({ element => 'relation', qualifier => 'ispartofseries', field => '082', subfield => 'a' })
      ->add_relation_ispartof
      ->add_format_mimetype
      ->add_single_subfield({ element => 'subject', qualifier => 'other', field => '630', subfield => 'a' })
      ->add_single_subfield({ element => 'subject', qualifier => 'other', field => '600', subfield => 'a' })
      ->add_subject_other_610;

    $self->xml->endTag('dublin_core');

    return $self->xml->end();
}

=head2 Internal methods

=head3 set_open

=cut

sub set_open {
    my ( $self ) = @_;

    my @fields_856 = $self->record->field('856');

    @fields_856 = grep {
              $_->subfield('u')
          and $_->subfield('u') =~ m{www2\.aladi\.org/biblioteca}
    } @fields_856;

    unless (@fields_856) {
        $self->open(0);
        return $self;
    }

    if ( any { $_->subfield('x') and $_->subfield('x') eq 'R' } @fields_856 ) {
        $self->open(0);
    }
    else {
        $self->open(1);
    }

    return $self;
}

=head3 contributor_mapping

=cut

sub contributor_mapping {
    return {
        'colab'     => 'collaborator',
        'consultor' => 'author',
        'coord'     => 'coordinator',
        'diseño'    => 'designeditor',
        'dir'       => 'director',
        'ed'        => 'editor',
        'il'        => 'illustrator',
        'superv'    => 'supervisor',
        'tr'        => 'translator',
    };
}

=head3 add_element

    $self->add_element(
        {
            element   => $element,
            qualifier => $qualifier,
            value     => $value,
          [ language  => $language,
            default_language => 0/1 ]
        }
    );

=cut

sub add_element {
    my ( $self, $params ) = @_;

    foreach my $param (qw(element qualifier value)) {
        die "$param is mandatory"
          unless exists $params->{$param};
    }

    my $language;

    if ( $params->{language} ) {
        $language = $params->{language};
    }
    elsif ( $params->{default_language} ) {
        $language = $self->language;
    }

    if ( $language ) {
        $self->xml->startTag(
            'dcvalue',
            element   => $params->{element},
            qualifier => $params->{qualifier},
            language  => $language
        );
    }
    else {
        $self->xml->startTag(
            'dcvalue',
            element   => $params->{element},
            qualifier => $params->{qualifier},
        );
    }
    $self->xml->characters( $params->{value} );
    $self->xml->endTag('dcvalue');

    return $self;
}

=head3 add_single_subfield

    $self->add_single_subfield(
        {
            element   => $element,
            qualifier => $qualifier,
            field     => $field,
            subfield  => $subfield,
          [ language  => $language,
            default_language => 1/0 ]
        }
    );

=cut

sub add_single_subfield {
    my ( $self, $params ) = @_;

    foreach my $param (qw(element qualifier field subfield)) {
        die "$param is mandatory"
          unless exists $params->{$param};
    }

    my @fields = $self->record->field( $params->{field} );

    foreach my $field (@fields) {

        my $value = $field->subfield( $params->{subfield} );

        next unless $value;

        $value = trim($value);
        $value =~ s/,$//;

        $value =~ s/\.$//
          if $params->{remove_dot};

        next unless $value;

        $self->add_element(
            {   element   => $params->{element},
                qualifier => $params->{qualifier},
                value     => $value,
                language  => $params->{language},
                default_language => $params->{default_language}
            }
        );
    }

    return $self;
}

=head3 add_title

    $self->add_title;

=cut

sub add_title {
    my ($self) = @_;

    my $field_245 = $self->record->field('245');

    return
      unless $field_245;

    my $a  = $field_245->subfield('a');
    $a = trim($a);
    $a =~ s/\s*\/$//;
    my @bs = map { chomp; $_ =~ s/\s*\///; $_ } $field_245->subfield('b');

    # cleanup
    chomp($a);

    my $value = join( ' ', $a, @bs );

    $self->add_element(
        {   element   => 'title',
            qualifier => undef,
            value     => $value,
            language  => 'es',
        }
    );

    return $self;
}

=head3 add_title_alternative

    $self->add_title_alternative;

=cut

sub add_title_alternative {
    my ($self) = @_;

    my $field_246 = $self->record->field('246');

    return $self
      unless $field_246;

    my $a  = $field_246->subfield('a');
    my @bs = map { chomp; $_ =~ s/\s*\///; $_ } $field_246->subfield('b');

    # cleanup
    chomp($a);

    my $value = join( ' ', $a, @bs );

    $self->add_element(
        {   element   => 'title',
            qualifier => 'alternative',
            value     => $value,
            language  => 'pt',
        }
    );

    return $self;
}

=head3 add_format_extent

    $self->add_format_extent;

=cut

sub add_format_extent {
    my ($self) = @_;

    my $field_300 = $self->record->field('300');

    return $self
      unless $field_300;

    my $a  = $field_300->subfield('a');
    my @bs = map { chomp; $_ } $field_300->subfield('b');
    my @es = map { chomp; $_ } $field_300->subfield('e');

    # cleanup
    chomp($a);

    my $value = join( ' ', $a, @bs, @es );

    $self->add_element(
        {   element   => 'format',
            qualifier => 'extent',
            value     => $value,
            language  => 'es',
        }
    );

    return $self;
}

=head3 add_current_date

    $self->add_current_date;

=cut

sub add_current_date {
    my ($self) = @_;

    my $date = DateTime->today->ymd;

    $self->add_element(
        {   element   => 'date',
            qualifier => 'accessioned',
            value     => $date,
        }
    );

    $self->add_element(
        {   element   => 'date',
            qualifier => 'available',
            value     => $date,
        }
    );

    return $self;
}

=head3 add_date_issued

    $self->add_date_issued;

=cut

sub add_date_issued {
    my ($self) = @_;

    my $field_033 = $self->record->field('033');

    return $self
      unless $field_033;

    my $a = $field_033->subfield('a');
    return
      unless $a and length($a) >= 8;

    my $year  = substr( $a, 0, 4 );
    my $month = substr( $a, 4, 2 );
    my $day   = substr( $a, 6, 2 );

    my $value = "$year-$month-$day";

    $self->add_element(
        {   element   => 'date',
            qualifier => 'issued',
            value     => $value,
        }
    );

    return $self;
}

=head3 add_contributor_author

    $self->add_contributor_author;

=cut

sub add_contributor_author {
    my ($self) = @_;

    my @fields_100 = $self->record->field('100');
    my @fields_700 = $self->record->field('700');

    # skip if $e present
    my @authors = grep { !$_->subfield('e') } ( @fields_100, @fields_700 );

    foreach my $author (@authors) {
        my $value = $author->subfield('a');

        next unless $value;

        $self->add_element(
            {   element   => 'contributor',
                qualifier => 'author',
                value     => $value,
            }
        );
    }

    my @fields_110 = $self->record->field('110');
    my @fields_710 = $self->record->field('710');

    # skip if $e present
    my @corporate_authors = grep { !$_->subfield('e') } ( @fields_110, @fields_710 );

    foreach my $author (@corporate_authors) {
        my $value = $author->subfield('a');

        next unless $value;

        my @bs = $author->subfield('b');
        $value = join( ' ', $value, map { trim($_) } @bs );

        $self->add_element(
            {   element   => 'contributor',
                qualifier => 'author',
                value     => $value,
            }
        );
    }

    return $self;
}

=head3 add_contributor

    $self->add_contributor( $e );

=cut

sub add_contributor {
    my ( $self, $e ) = @_;

    my $mapping = $self->contributor_mapping;

    die "\$e ($e) must be mapped."
      unless any { $_ eq $e } keys %{$mapping};

    my $qualifier = $mapping->{$e};

    my @fields_100 = $self->record->field('100');
    my @fields_700 = $self->record->field('700');

    # filter by $e
    my @authors = grep { $_->subfield('e') && $_->subfield('e') eq $e } ( @fields_100, @fields_700 );

    foreach my $author (@authors) {
        my $value = $author->subfield('a');

        next unless $value;

        $self->add_element(
            {   element   => 'contributor',
                qualifier => $qualifier,
                value     => $value,
            }
        );
    }

    my @fields_110 = $self->record->field('110');
    my @fields_710 = $self->record->field('710');

    # filter by $e
    my @corporate_authors = grep { $_->subfield('e') && $_->subfield('e') eq $e } ( @fields_110, @fields_710 );

    foreach my $author (@corporate_authors) {
        my $value = $author->subfield('a');

        next unless $value;

        my @bs = $author->subfield('b');
        $value = join( ' ', $value, map { trim($_) } @bs );

        $self->add_element(
            {   element   => 'contributor',
                qualifier => $qualifier,
                value     => $value,
            }
        );
    }

    return $self;
}

=head3 add_contributor_other

    $self->add_contributor_other;

=cut

sub add_contributor_other {
    my ($self) = @_;

    my $mapping   = $self->contributor_mapping;
    my $qualifier = 'other';

    my @fields_100 = $self->record->field('100');
    my @fields_700 = $self->record->field('700');

    my @authors = grep { $_->subfield('e') } ( @fields_100, @fields_700 );

    foreach my $author (@authors) {

        next unless none { $author->subfield('e') eq $_ } keys %{$mapping};

        my $value = $author->subfield('a');

        next unless $value;

        $self->add_element(
            {   element   => 'contributor',
                qualifier => $qualifier,
                value     => $value,
            }
        );
    }

    my @fields_110 = $self->record->field('110');
    my @fields_710 = $self->record->field('710');

    # skip if $e present
    my @corporate_authors = grep { $_->subfield('e') } ( @fields_110, @fields_710 );

    foreach my $author (@corporate_authors) {

        next unless none { $author->subfield('e') eq $_ } keys %{$mapping};

        my $value = $author->subfield('a');

        next unless $value;

        my @bs = $author->subfield('b');
        $value = join( ' ', $value, map { trim($_) } @bs );

        $self->add_element(
            {   element   => 'contributor',
                qualifier => $qualifier,
                value     => $value,
            }
        );
    }

    return $self;
}

=head3 add_contributor_conference

    $self->add_contributor_conference;

=cut

sub add_contributor_conference {
    my ($self) = @_;

    my @fields_111 = $self->record->field('111');
    my @fields_711 = $self->record->field('711');

    my @conferences = ( @fields_111, @fields_711 );

    foreach my $author (@conferences) {

        my $value = $author->subfield('a');

        next unless $value;

        my $n = $author->subfield('n');
        my $d = $author->subfield('d');
        my $c = $author->subfield('c');

        my @reminder = ();
        push( @reminder, $n ) if $n;
        push( @reminder, $d ) if $d;
        push( @reminder, $c ) if $c;

        if (@reminder) {
            $value .= ' ' . join( ' ', @reminder );
        }

        $self->add_element(
            {   element   => 'contributor',
                qualifier => 'conferencename',
                value     => $value,
            }
        );
    }

    return $self;
}

=head3 add_coverage_spatial

    $self->add_coverage_spatial;

=cut

sub add_coverage_spatial {
    my ($self) = @_;

    return $self->add_single_subfield(
        {   element   => 'coverage',
            qualifier => 'spatial',
            field     => '651',
            subfield  => 'a',
            language  => 'es',
        }
    );
}

=head3 add_date_norspa

    $self->add_date_norspa;

=cut

sub add_date_norspa {
    my ($self) = @_;

    return $self->add_single_subfield(
        {   element    => 'date',
            qualifier  => 'norspa',
            field      => '260',
            subfield   => 'c',
            language   => 'es',
            remove_dot => 1,
        }
    );
}

=head3 add_description

    $self->add_description;

=cut

sub add_description {
    my ($self) = @_;

    return $self->add_single_subfield(
        {   element   => 'description',
            qualifier => undef,
            field     => '500',
            subfield  => 'a'
        }
    )->add_single_subfield(
        {   element   => 'description',
            qualifier => undef,
            field     => '504',
            subfield  => 'a'
        }
    );
}

=head3 add_description_abstract

    $self->add_description_abstract;

=cut

sub add_description_abstract {
    my ($self) = @_;

    return $self->add_single_subfield(
        {   element   => 'description',
            qualifier => 'abstract',
            field     => '520',
            subfield  => 'a'
        }
    );
}

=head3 add_description_tableofcontents

    $self->add_description_tableofcontents;

=cut

sub add_description_tableofcontents {
    my ($self) = @_;

    return $self->add_single_subfield(
        {   element   => 'description',
            qualifier => 'tableofcontents',
            field     => '505',
            subfield  => 'a'
        }
    );
}

=head3 add_33x

    $self->add_33x( $field );

=cut

sub add_33x {
    my ( $self, $field ) = @_;

    my @fields_33x = $self->record->field($field);

    foreach my $f_33x (@fields_33x) {

        my $a = $f_33x->subfield('a');
        my $b = $f_33x->subfield('b');
        my $THE_2 = $f_33x->subfield('2');

        next unless $a;

        my $remainder = join( '/', ($b, $THE_2));

        my $value = ($b) ? "$a ($b)" : $a;

        $self->add_element(
            {   element   => 'format',
                qualifier => $field . 'marc',
                value     => $value,
                language  => 'es'
            }
        );
    }

    return $self;
}

=head3 add_identifier_isbn

    $self->add_identifier_isbn;

=cut

sub add_identifier_isbn {
    my ($self) = @_;

    my @fields_020 = $self->record->field('020');

    foreach my $f_020 (@fields_020) {

        my $a = $f_020->subfield('a');
        my $c = $f_020->subfield('c');

        next unless $a;

        my $value = ($c) ? "$a ($c)" : $a;

        $self->add_element(
            {   element   => 'identifier',
                qualifier => 'isbn',
                value     => $value,
            }
        );
    }

    return $self;
}

=head3 add_identifier_other

    $self->add_identifier_other;

=cut

sub add_identifier_other {
    my ($self) = @_;

    my $biblionumber = $self->record->subfield( '999', 'c' );

    die 'biblionumber mandatory'
      unless $biblionumber;

    $self->add_element(
        {
            element   => 'identifier',
            qualifier => 'other',
            value     => "biblionumber=$biblionumber",
        }
    );

    return $self;
}

=head3 add_coverage_temporal

    $self->add_coverage_temporal;

=cut

sub add_coverage_temporal {
    my ($self) = @_;

    return $self->add_single_subfield(
        {   element   => 'coverage',
            qualifier => 'temporal',
            field     => '648',
            subfield  => 'a'
        }
    );
}

=head3 add_language_iso

    $self->add_language_iso;

=cut

sub add_language_iso {
    my ($self) = @_;

    my @fields_041 = $self->record->field('041');

    my $to_iso = {
        en => 'eng',
        es => 'spa',
        pt => 'por',
    };

    foreach my $language (@fields_041) {
        my @subfields = $language->subfield('a');
        next unless @subfields;

        foreach my $value ( @subfields ) {
            unless ( any { $value eq $_ } keys %{ $to_iso } ) {
                warn "$value is not a valid language";
                next;
            }

            $self->add_element(
                {   element   => 'language',
                    qualifier => 'iso',
                    value     => $to_iso->{$value},
                    language  => 'en',
                }
            );
        }
    }

    return $self;
}

=head3 add_rights

    $self->add_rights;

=cut

sub add_rights {
    my ($self) = @_;

    if ( $self->open ) {
        $self->add_element(
            {   element   => 'rights',
                qualifier => undef,
                value     => 'Attribution-NonCommercial-NoDerivatives 4.0 Internacional',
            }
        );
        $self->add_element(
            {   element   => 'rights',
                qualifier => 'uri',
                value     => 'http://creativecommons.org/licenses/by-nc-nd/4.0/',
            }
        );
        $self->add_element(
            {   element   => 'rights',
                qualifier => 'accessLevel',
                value     => 'openAccess',
                language  => 'en',
            }
        );
    }
    else {
        $self->add_element(
            {   element   => 'rights',
                qualifier => undef,
                value     => 'Restringido para uso exclusivo de las Representaciones Permanentes ante la ALADI',
                language  => 'es',
            }
        );
        $self->add_element(
            {   element   => 'rights',
                qualifier => 'uri',
                value     => 'https://hdl.handle.net/20.500.12909/90',
            }
        );
        $self->add_element(
            {   element   => 'rights',
                qualifier => 'accessLevel',
                value     => 'restrictedAccess',
                language  => 'en',
            }
        );
    }

    return $self;
}

=head3 add_subject

    $self->add_subject;

=cut

sub add_subject {
    my ($self) = @_;

    my @fields_650 = $self->record->field('650');

    foreach my $subject (@fields_650) {

        my $link = $subject->subfield('9');

        next unless $link;

        my $auth = Koha::MetadataRecord::Authority->get_from_authid($link)->record;

        my $thesaur = $auth->subfield( '670', 'a' );

        my $mapping = {
            'Keywords'           => 'keywords',
            'Macrothesauro OCDE' => 'macro',
            'Tesauro UNBIS'      => 'unbis',
            'ALADI/VCB/1'        => 'aladi',
        };

        my $qualifier = $mapping->{$thesaur} // 'ERROR';

        my $value = $auth->subfield( '150', 'a' );

        $self->add_element(
            {   element   => 'subject',
                qualifier => $qualifier,
                value     => $value,
                language  => 'es',
            }
        );

        my @f_450s = $auth->field('450');

        if (@f_450s) {
            $self->add_element(
                {   element   => 'subject',
                    qualifier => $qualifier,
                    value     => $f_450s[0]->subfield('a'),
                    language  => 'en',
                }
            );

            $self->add_element(
                {   element   => 'subject',
                    qualifier => $qualifier,
                    value     => $f_450s[1]->subfield('a'),
                    language  => 'pt',
                }
            );
        }
    }

    return $self;
}

=head3 add_subject_genreform

    $self->add_subject_genreform;

=cut

sub add_subject_genreform {
    my ($self) = @_;

    my @fields_655 = $self->record->field('655');

    foreach my $subject (@fields_655) {

        my $link = $subject->subfield('9');

        next unless $link;

        my $auth = Koha::MetadataRecord::Authority->get_from_authid($link)->record;

        unless ($auth) {
            warn "Wrong auth link: authid - $link";
            next;
        }

        my $value = $auth->subfield( '155', 'a' );

        $self->add_element(
            {   element   => 'subject',
                qualifier => 'genreform',
                value     => $value,
                language  => 'es',
            }
        );

        my @f_455s = $auth->field('455');

        $self->add_element(
            {   element   => 'subject',
                qualifier => 'genreform',
                value     => $f_455s[0]->subfield('a'),
                language  => 'en',
            }
        );

        $self->add_element(
            {   element   => 'subject',
                qualifier => 'genreform',
                value     => $f_455s[1]->subfield('a'),
                language  => 'pt',
            }
        );
    }

    return $self;
}

=head3 add_relation_ispartofseries

    $self->add_relation_ispartofseries;

=cut

sub add_relation_ispartofseries {
    my ($self) = @_;

    my @fields_490 = $self->record->field('490');

    foreach my $subject (@fields_490) {

        my @as = $subject->subfield('a');
        my @vs = $subject->subfield('v');

        my $value = join( ' ', @as, @vs );

        next unless $value;

        $self->add_element(
            {   element   => 'relation',
                qualifier => 'ispartofseries',
                value     => $value,
            }
        );
    }

    return $self;
}

=head3 add_relation_ispartof

    $self->add_relation_ispartof;

=cut

sub add_relation_ispartof {
    my ($self) = @_;

    my @fields_773 = $self->record->field('773');

    foreach my $subject (@fields_773) {

        my $t = $subject->subfield('t');
        my $a = $subject->subfield('a');
        my $b = $subject->subfield('b');
        my $d = $subject->subfield('d');
        my $g = $subject->subfield('g');

        my $remainder = join( ' ', ($a, $b, $d, $g) );

        my $value;

        if ( $t and $remainder ) {
            if ( $a ) {
                $value = "$t / $remainder";
            }
            else {
                $value = "$t" . ". " . "$remainder";
            }
        }
        elsif ( $t ) {
            $value = $t;
        }
        elsif ( $remainder ) {
            $value = $remainder;
        }

        next unless $value;

        $self->add_element(
            {   element   => 'relation',
                qualifier => 'ispartof',
                value     => $value,
            }
        );
    }

    return $self;
}

=head3 add_type

    $self->add_type;

=cut

sub add_type {
    my ($self) = @_;

    my $field_082_a = $self->record->subfield( '082', 'a' );
    my $field_856_u = $self->record->subfield( '856', 'u' );
    my $field_942_c = $self->record->subfield( '942', 'c' );

    my $value = 'other';

    if (
        $field_082_a
        and (  $field_082_a =~ m{^SEC/dt}
            or $field_082_a =~ m{^CR/dt} )
      )
    {
        $value = 'workingPaper';
    }
    elsif (
            $field_082_a
        and $field_082_a =~ m{^SEC/Estudio}

      )
    {
        $value = 'report';
    }
    elsif ( $field_856_u
        and $field_856_u =~
        m{http://www2.aladi.org/biblioteca/Publicaciones/ALADI/Reuniones/} )
    {
        $value = 'conferenceObject';
    }
    elsif ( $field_942_c eq 'LIBRO' ) {
        $value = 'book';
    }
    elsif ( $field_942_c eq 'ALIBRO' ) {
        $value = 'bookPart';
    }

    $self->add_element(
        {
            element   => 'type',
            qualifier => undef,
            value     => $value,
            language  => 'en',
        }
    );

    return $self;
}

=head3 add_type_local

    $self->add_type_local;

=cut

sub add_type_local {
    my ($self) = @_;

    my $field_856_u = $self->record->subfield( '856', 'u' );

    my $value;

    if ( $field_856_u =~
        m{http://www2.aladi.org/biblioteca/Publicaciones/ALADI/Acuerdos/} )
    {
        $value = 'Acuerdo';
    }
    elsif ( $field_856_u =~
        m{http://www2.aladi.org/biblioteca/Publicaciones/ALADI/Nomeclatura_ALADI/} )
    {
        $value = 'Nomenclatura';
    }
    elsif ( $field_856_u =~
        m{http://www2.aladi.org/biblioteca/Publicaciones/ALADI/Comite_de_Representantes/CR_Actas/} )
    {
        $value = 'Actas';
    }
    elsif ( $field_856_u =~
        m{http://www2.aladi.org/biblioteca/Publicaciones/ALADI/Comite_de_Representantes/CR_Grupos_De_Trabajo/} )
    {
        $value = 'Minuta';
    }
    elsif ( $field_856_u =~
        m{http://www2.aladi.org/biblioteca/Publicaciones/ALADI/Comite_de_Representantes/CR_Acuerdos/} )
    {
        $value = 'Acuerdo del Comité de Representantes';
    }
    elsif ( $field_856_u =~
        m{http://www2.aladi.org/biblioteca/Publicaciones/ALADI/Comite_de_Representantes/CR_Resoluciones/} )
    {
        $value = 'Resolución';
    }
    elsif ( $field_856_u =~
        m{http://www2.aladi.org/biblioteca/Publicaciones/ALADI/Consejo_de_Ministros/CR_Resoluciones/} )
    {
        $value = 'Resolución';
    }
    elsif ( $field_856_u =~
        m{http://www2.aladi.org/biblioteca/Publicaciones/ALADI/Comite_de_Representantes/CR_di/} )
    {
        $value = 'Documento informativo';
    }
    elsif ( $field_856_u =~
        m{http://www2.aladi.org/biblioteca/Publicaciones/ALADI/Secretaria_General/SEC_di/} )
    {
        $value = 'Documento informativo';
    }

    $self->add_element(
        {
            element   => 'type',
            qualifier => 'local',
            value     => $value,
            language  => 'es',
        }
    ) if $value;

    return $self;
}

=head3 add_type_oaireResourceType

    $self->add_type_oaireResourceType;

=cut

sub add_type_oaireResourceType {
    my ($self) = @_;

    my $field_856_u = $self->record->subfield( '856', 'u' );
    my $field_942_c = $self->record->subfield( '942', 'c' );

    my $value = 'text';

    if ( $field_856_u =~
        m{http://www2.aladi.org/biblioteca/Publicaciones/ALADI/Observatorio/Boletines_Estadisticos/} )
    {
        $value = 'journal';
    }
    elsif ( $field_856_u =~
        m{http://www2.aladi.org/biblioteca/Publicaciones/ALADI/Secretaria_General/SEC_Memorandum/} )
    {
        $value = 'memorandum';
    }
    elsif ( $field_942_c eq 'VIDEO' ) {
        $value = 'video';
    }

    $self->add_element(
        {
            element   => 'type',
            qualifier => 'oaireResourceType',
            value     => $value,
            language  => 'en',
        }
    ) if $value;

    return $self;
}

=head3 add_format_mimetype

    $self->add_format_mimetype;

=cut

sub add_format_mimetype {
    my ($self) = @_;

    my @fields_856 = $self->record->field( '856' );

    my $mimetype = 'application/pdf';

    unless ( any { $_->subfield('u') =~ m/\.pdf/i } @fields_856 ) {
        # word, excel, zip

        if ( any { $_->subfield('u') =~ m/\.doc|\.docx/i } @fields_856 ) {
            $mimetype = "application/msword";
        }
        elsif ( any { $_->subfield('u') =~ m/\.xls|\.xslx/i } @fields_856 ) {
            $mimetype = "application/ms-excel";
        }
        elsif ( any { $_->subfield('u') =~ m/\.zip/i } @fields_856 ) {
            $mimetype = "application/zip";
        }
    }

    $self->add_element(
        {
            element   => 'format',
            qualifier => 'mimetype',
            value     => $mimetype,
        }
    );

    return $self;
}

=head3 add_subject_other_610

    $self->add_subject_other_610;

=cut

sub add_subject_other_610 {
    my ($self) = @_;

    my @fields_610 = $self->record->field('610');

    foreach my $author (@fields_610) {
        my $value = $author->subfield('a');

        next unless $value;

        my @bs = $author->subfield('b');
        $value = join( ' ', $value, map { trim($_) } @bs );

        $self->add_element(
            {   element   => 'subject',
                qualifier => 'other',
                value     => $value,
            }
        );
    }

    return $self;
}

sub StripNonXmlChars {
    my $str = shift;
    if ( !defined($str) || $str eq "" ) {
        return "";
    }
    $str =~ s/[^\x09\x0A\x0D\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}\x{10000}-\x{10FFFF}]//g;
    return $str;
}

sub trim {
    my ($string) = @_;

    if ( defined $string ) {
        $string =~ s/\s*$//;
        $string =~ s/^\s*//;
    }

    return $string;
}

1;
