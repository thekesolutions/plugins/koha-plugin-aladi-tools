package Koha::Plugin::Com::Theke::ALADI;

# Copyright 2022 Theke Solutions
#
# This file is part of koha-plugin-aladi-tools.
#
# koha-plugin-aladi-tools is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# koha-plugin-aladi-tools is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with koha-plugin-aladi-tools; if not, see <http://www.gnu.org/licenses>.

use Modern::Perl;

use utf8;

use base qw(Koha::Plugins::Base);

use Mojo::JSON qw(decode_json);

BEGIN {
    my $path = Module::Metadata->find_module_by_name(__PACKAGE__);
    $path =~ s!\.pm$!/lib!;
    unshift @INC, $path;
}

our $VERSION = "{VERSION}";

our $metadata = {
    name            => 'ALADI Tools for Koha',
    author          => 'Tomas Cohen Arazi',
    description     => 'Tools used by ALADI',
    date_authored   => '2020-06-11',
    date_updated    => "1900-01-01",
    minimum_version => '21.0500000',
    maximum_version => undef,
    version         => $VERSION,
};

=head1 METHODS

=head2 new

Plugin object constructor

=cut

sub new {
    my ( $class, $args ) = @_;

    ## We need to add our metadata here so our base class can access it
    $args->{'metadata'} = $metadata;

    my $self = $class->SUPER::new($args);

    return $self;
}

=head3 api_routes

Method that returns the API routes to be merged into Koha's

=cut

sub api_routes {
    my ( $self, $args ) = @_;

    my $spec_str = $self->mbf_read('openapi.json');
    my $spec     = decode_json($spec_str);

    return $spec;
}

=head3 api_namespace

Method that returns the namespace for the plugin API to be put on

=cut

sub api_namespace {
    my ( $self ) = @_;
    
    return 'aladi';
}

=head3 intranet_js

=cut

sub intranet_js {
    return q{
<script>
    $(document).ready(function(){
        $('#catalog_detail').ready(function() {
             let biblio_id = $('input[type="hidden"][name="bib"]').val();
             $("#toolbar").append('<div class="btn-group"><a class="btn btn-default" href="/api/v1/contrib/aladi/biblios/'+biblio_id+'/bundle"><i class="fa fa-download"></i> Bundle DSpace</a></div>');
         });
    });
</script>
};
}

1;
