#!/usr/bin/perl

use Modern::Perl;

use utf8;

use File::Basename;
use File::Copy qw(copy);
use Getopt::Long;
use JSON qw( from_json );
use LWP::Simple qw( get );
use Text::CSV;

use Data::Printer colored => 1;

binmode STDOUT, ':encoding(UTF-8)';
binmode STDERR, ':encoding(UTF-8)';

my $help;
my $verbose;
my $file;
my $limit;
my $dir;
my $files_dir;
my $restricted_access_group = 'Funcionarios';
my $log_file;
my $tmp;

GetOptions(
    'h|help'      => \$help,
    'v|verbose'   => \$verbose,
    'f|file:s'    => \$file,
    'files_dir:s' => \$files_dir,
    'l|limit:i'   => \$limit,
    'd|dir:s'     => \$dir,
    'r:s'         => \$restricted_access_group,
    'l|log|:s'    => \$log_file,
    'tmp:s'       => \$tmp,
);

my $usage = << 'ENDUSAGE';

This script changes bibliographic records in Koha based on
a configuration file and a CSV

This script has the following parameters :
    -h --help:  this message
    -f --file:  the CSV file to use
    -l --limit: how many rows to process
    -v --verbose

ENDUSAGE

if ($help) {
    print $usage;
    exit;
}

unless ($file) {
    print STDERR "-f or --file is mandatory \n$usage";
    exit 1
}

unless ($dir) {
    print STDERR "-d or --dir is mandatory \n$usage";
    exit 1
}

unless ($files_dir) {
    print STDERR "--files_dir is mandatory \n$usage";
    exit 1
}

my $log_fh;

if ($log_file) {
    open($log_fh, ">>:encoding(UTF-8)", "$log_file")
      or die "Can't open $log_file: $!";
}

# Open the file
my $csv = Text::CSV->new ({ binary => 1, auto_diag => 1, sep_char => "," });
open my $fh, "<", $file or die "file: $!";

my $counter = 0;
my $success = 0;
my $failed  = 0;

while ( my $row = $csv->getline ($fh) ) {

    $counter++;

    my $biblio_id  = $row->[0];
    my $collection = $row->[1];

    my ( $handle, $collection_id ) = split( '/', $collection );

    print STDOUT "biblio_id: $biblio_id | collection: $collection\n";

    my $dublincore = get( "https://aladi-admin.koha.theke.io/api/v1/contrib/aladi/biblios/$biblio_id/dublin_core" );
    my $local      = get( "https://aladi-admin.koha.theke.io/api/v1/contrib/aladi/biblios/$biblio_id/local" );
    my $urls       = from_json ( get( "https://aladi-admin.koha.theke.io/api/v1/contrib/aladi/biblios/$biblio_id/urls" ) );

    my @files;

    my $file_error;

    my $ext_map = {
        pdf => 'PDF',
        PDF => 'pdf',
        doc => 'DOC',
        DOC => 'doc',
        xls => 'XLS',
        XLS => 'xls',
        zip => 'ZIP',
        ZIP => 'zip',
    };

    foreach my $url ( @{$urls} ) {

        my $file = $url->{url};
        my $dest_name = $url->{file_name};

        $file =~ s|http://www2\.aladi\.org/biblioteca/Publicaciones/||;
        $file = trim($file);
        my $restricted = ($url->{restricted}) ? 1 : 0;
        my ($filename) = fileparse($file);

        my $full_file = "$files_dir/$file";
        my $original_name = $full_file;

        if ( -e $full_file ) {
            unless ( copy( $full_file, "$tmp/$dest_name" ) ) {
                $file_error = $full_file;
                last;
            }

            unlink( "$tmp/$dest_name" );
        }
        else {

            if ( $full_file =~ m/\.(?<extension>pdf|PDF|doc|DOC|zip|ZIP|xls|XLS)$/ ) {
                my $extension     = $+{extension};
                my $new_extension = $ext_map->{$extension};

                $full_file =~ s/\.$extension/\.$new_extension/;
                if ( -e $full_file ) {
                    $filename =~ s/\.$extension/\.$new_extension/;
                    unless ( copy( $full_file, "$tmp/$dest_name" ) ) {
                        $file_error = $full_file;
                        last;
                    }

                    unlink( "$tmp/$dest_name" );
                }
                else {
                    $file_error = $full_file;
                    last;
                }
            }
            else {
                $file_error = $full_file;
                last;
            }
        }

        push @files, { file => $full_file, restricted => $restricted, filename => $dest_name };
    }

    if ( $file_error ) {
        # logging
        print $log_fh "$biblio_id: error copying one or more files ($file_error)\n";
        $file_error = undef;
        next;
    }

    my $dest_dir = "$dir/$biblio_id/$biblio_id";

    if ( mkdir( "$dir/$biblio_id" ) and mkdir( "$dir/$biblio_id/$biblio_id" ) ) {

        open( my $dc_handle, ">:encoding(UTF-8)", "$dest_dir/dublin_core.xml")
            or die "Can't save > $dest_dir/dublin_core.xml: $!";

        open( my $local_handle, ">:encoding(UTF-8)", "$dest_dir/metadata_local.xml")
            or die "Can't save > $dest_dir/metadata_local.xml: $!";

        open( my $license_handle, ">:encoding(UTF-8)", "$dest_dir/license.txt")
            or die "Can't save > $dest_dir/license.txt: $!";

        open( my $collections_handle, ">:encoding(UTF-8)", "$dest_dir/collections")
            or die "Can't save > $dest_dir/collections: $!";

        open( my $contents_handle, ">>:encoding(UTF-8)", "$dest_dir/contents")
            or die "Can't save > $dest_dir/contents: $!";

        foreach my $file ( @files ) {

            my $full_file = $file->{file};
            my $filename  = $file->{filename};

            if ( -e $full_file ) {
                unless ( copy( $full_file, "$dest_dir/$filename" ) ) {
                    die "Error copying $full_file to $dest_dir: $!";
                }
            }

            if ( $file->{restricted} ) {
                print $contents_handle "$filename\tpermissions:-r '$restricted_access_group'\n";
            }
            else {
                print $contents_handle "$filename\tbundle:ORIGINAL\n";
            }
        }

        print $dc_handle $dublincore;
        print $local_handle $local;
        print $license_handle license();
        print $collections_handle "$collection\n";

        print $contents_handle "license.txt\tbundle:LICENSE\n";

        close($dc_handle);
        close($local_handle);
        close($license_handle);
        close($collections_handle);
        close($contents_handle);

        $success++;
    }
    else {
        print STDERR "Error creating $dir/$collection_id";
    }

    last
      if defined $limit and $counter == $limit;
}

close($log_fh);
close($fh);

sub license {
    my $license = <<LICENSE;
Licencia de Distribución no exclusiva de los documentos en el Repositorio Institucional de la ALADI.


1. Declaración de titularidad

La ALADI, declara que los Documentos Oficiales de su Repositorio, constituyen trabajos originales, que no infringen los derechos de autor de ninguna otra persona o entidad.

En caso de ser co-titular, declara que cuenta con el consentimiento de los restantes titulares para hacer la presente cesión.

En caso de previa cesión de derechos de explotación sobre la obra a terceros, declara que tien la autorización expresa de aquellos a los fines de esta cesión, conservando la facultad de ceder estos derechos. Además, deja constancia, que se han adquirido todos los derechos o autorizaciones necesarias por las contribuciones de terceros al contenido del trabajo y que éstos están claramente identificados y reconocidos en el mismo.

En el caso de que el documento haya sido subsidiado por acuerdos con otros Organismos distintos a la ALALDI, declara que cumple con las obligaciones exigidas por tales acuerdos.

2. Derechos patrimoniales de los Documentos Oficiales cedidos al Repositorio Institucional de la ALADI

Para que el Repositorio Institucional de la ALADI pueda reproducir y comunicar públicamente su acervo documental, la ALADI cumple con las siguientes condiciones:

-> Como autor, la ALADI tiene el derecho gratuito y no exclusivo de archivar, reproducir, convertir, comunicar y/o distribuir los Documentos Oficiales a nivel internacional, en formato electrónico.

-> Se acuerda que la ALADI pueda conservar más de una copia de los documentos y, sin alterar su contenido, convertirlo a cualquier formato de fichero, medio o soporte, para propósitos de seguridad, preservación y acceso.

3. Condiciones de uso

Los documentos que integran el Repositorio Institucional de la ALADI pueden ser de dos categorías:

a) Aquellos de uso interno o "Restringidos para uso exclusivo de las Representaciones"; y
b) Aquellos que poseen licencias para abrir los archivos al uso público.

Estos últimos estarán disponibles al público para que se haga un uso justo y respetuoso de los derechos de autor, siendo requisito indispensable, citar la fuente, reconocer la autoría de quien firma los documentos y cumplir con las condiciones de la Licencia de Distribución No Exclusiva (Licencia Creative Commons).

Abril de 2022.

LICENSE

    return $license;
}

sub trim {
    my ($string) = @_;

    $string =~ s/\s*$//;
    $string =~ s/^\s*//;
    $string =~ s/\n//;

    return $string;
}

1;
